﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatzAngular.Model
{
    public class People
    {
    public string Name { get; set; }
    public string Gender { get; set; }
    public int Age { get; set; }
    public Pet[] Pets { get; set; }
  }
}
