﻿using CatzAngular.Model;
using CatzAngular.Repo;
using CatzAngular.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Catz.WebApi.Controllers
{
  [Route("api/[controller]")]
  public class PeopleController : ApiController
  {
    private PeopleService peopleService;

    public PeopleController()
    {
      this.peopleService = new PeopleService(new PeopleRepo());
    }

    [HttpGet, Route("getPeople")]
    public People[] GetPeople()
    {
      return this.peopleService.GetAll();
    }
  }
}
