﻿using CatzAngular.IRepo;
using CatzAngular.Model;
using CatzAngular.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatzAngular.Services
{
  public class PeopleService
  {
    private IPeopleRepo peopleRepo;

    public PeopleService(IPeopleRepo peopleRepo)
    {
      this.peopleRepo = peopleRepo;
    }

    public People[] GetAll()
    {
      return peopleRepo.GetAll();
    }
  }
}
