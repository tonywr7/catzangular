import { Routes } from '@angular/router'
import { Error404Component } from './errors/404.component'

import {
  CatzListResolver,
  CatzListComponent
} from './catz/index'

import {
  EventsListComponent,
  EventDetailsComponent,
  CreateEventComponent,
  //EventRouteActivator, No longer used *S97*
  EventListResolver,
  EventResolver,
  CreateSessionComponent,
  TestGridEventComponent,
  TestSignalRComponent,
  TestTinyMCEComponent
} from './events/index'

export const appRoutes: Routes = [
  { path: 'catz', component: CatzListComponent, resolve: { catzByGender: CatzListResolver } },
  { path: 'events', component: EventsListComponent, resolve: { events: EventListResolver } },
  { path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent'] },
  { path: 'events/testgrid', component: TestGridEventComponent },
  { path: 'events/testsignalr', component: TestSignalRComponent },
  { path: 'events/testtinymce', component: TestTinyMCEComponent },
  // { path: 'events/:id', component: EventDetailsComponent, canActivate: [EventRouteActivator]}, route activator no longer used, changed to resolve to preload data *S97*
  { path: 'events/:id', component: EventDetailsComponent, resolve: { event: EventResolver } },
  { path: 'events/session/new', component: CreateSessionComponent },
  { path: '404', component: Error404Component },
  { path: '', redirectTo: '/catz', pathMatch: 'full' },
  { path: 'user', loadChildren: 'app/user/user.module#UserModule' }

]