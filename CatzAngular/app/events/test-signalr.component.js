"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("./shared/index");
//import { EJ_TOKEN } from '../common/ej.service'
var jQuery_service_1 = require("../common/jQuery.service");
var TestSignalRComponent = (function () {
    function TestSignalRComponent(router, eventService, $) {
        this.router = router;
        this.eventService = eventService;
        this.$ = $;
        this.pagesize = 5;
        this.messages = ["First message", "Second message"];
        this.chatHub = null;
    }
    TestSignalRComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.name.nativeElement.value = 'Guest';
        this.message.nativeElement.value = 'Default message';
        this.chatHub = this.$.connection.chatHub;
        this.chatHub.client.broadcastMessage = function (name, message) {
            var newMessage = name + ' says: ' + message;
            console.log("got message");
            // push the newly coming message to the collection of messages
            _this.messages.push(newMessage);
            //this.$apply();
        };
        this.$.connection.hub.start();
        this.gridData = [{
                OrderID: 10248, CustomerID: 'VINET', EmployeeID: 5,
                OrderDate: new Date(8364186e5), Freight: 32.38
            },
            {
                OrderID: 10249, CustomerID: 'TOMSP', EmployeeID: 6,
                OrderDate: new Date(836505e6), Freight: 11.61
            },
            {
                OrderID: 10250, CustomerID: 'HANAR', EmployeeID: 4,
                OrderDate: new Date(8367642e5), Freight: 65.83
            },
            {
                OrderID: 10251, CustomerID: 'VICTE', EmployeeID: 3,
                OrderDate: new Date(8367642e5), Freight: 41.34
            },
            {
                OrderID: 10252, CustomerID: 'SUPRD', EmployeeID: 4,
                OrderDate: new Date(8368506e5), Freight: 51.3
            },
            {
                OrderID: 20248, CustomerID: 'AINET', EmployeeID: 5,
                OrderDate: new Date(8364186e5), Freight: 132.38
            },
            {
                OrderID: 20249, CustomerID: 'AOMSP', EmployeeID: 6,
                OrderDate: new Date(836505e6), Freight: 111.61
            },
            {
                OrderID: 20250, CustomerID: 'AANAR', EmployeeID: 4,
                OrderDate: new Date(8367642e5), Freight: 165.83
            },
            {
                OrderID: 20251, CustomerID: 'AICTE', EmployeeID: 3,
                OrderDate: new Date(8367642e5), Freight: 141.34
            },
            {
                OrderID: 20252, CustomerID: 'AUPRD', EmployeeID: 4,
                OrderDate: new Date(8368506e5), Freight: 151.3
            }];
        //var data = ej.DataManager(window["gridData"]).executeLocal(ej.Query().take(50));
        //this.$("#Grid").ejGrid({
        //  dataSource: data,
        //  allowPaging: true,
        //  columns: [
        //    { field: "OrderID", headerText: "Order ID", width: 75, textAlign: ej.TextAlign.Right },
        //    { field: "CustomerID", headerText: "Customer ID", width: 80 },
        //    { field: "EmployeeID", headerText: "Employee ID", width: 75, textAlign: ej.TextAlign.Right },
        //    { field: "Freight", width: 75, format: "{0:C}", textAlign: ej.TextAlign.Right },
        //    { field: "OrderDate", headerText: "Order Date", width: 80, format: "{0:MM/dd/yyyy}", textAlign: ej.TextAlign.Right },
        //    { field: "ShipCity", headerText: "Ship City", width: 110 }
        //  ]
        //});     
    };
    TestSignalRComponent.prototype.newMessage = function () {
        // sends a new message to the server
        this.chatHub.server.sendMessage(this.name.nativeElement.value, this.message.nativeElement.value);
        console.log("Sent message");
        this.message.nativeElement.value = '';
    };
    TestSignalRComponent.prototype.myclick = function () {
        console.log(this);
        console.log(this.$("#mygrid"));
        //console.log(this.mygrid.el.nativeElement)
    };
    return TestSignalRComponent;
}());
__decorate([
    core_1.ViewChild('mygrid'),
    __metadata("design:type", core_1.ElementRef)
], TestSignalRComponent.prototype, "mygrid", void 0);
__decorate([
    core_1.ViewChild("name"),
    __metadata("design:type", core_1.ElementRef)
], TestSignalRComponent.prototype, "name", void 0);
__decorate([
    core_1.ViewChild("message"),
    __metadata("design:type", core_1.ElementRef)
], TestSignalRComponent.prototype, "message", void 0);
TestSignalRComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/events/test-signalr.component.html'
    }),
    __param(2, core_1.Inject(jQuery_service_1.JQ_TOKEN)),
    __metadata("design:paramtypes", [router_1.Router, index_1.EventService, Object])
], TestSignalRComponent);
exports.TestSignalRComponent = TestSignalRComponent;
//# sourceMappingURL=test-signalr.component.js.map