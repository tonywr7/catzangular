import { Component, Inject, ElementRef, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { EventService } from './shared/index'
//import { EJ_TOKEN } from '../common/ej.service'
import { JQ_TOKEN } from '../common/jQuery.service'

declare let ej: any;
declare let tinymce: any;

@Component({
  templateUrl: 'app/events/test-tinymce.component.html',
  styles: [`
    blockquote {
      padding-left: 4px;
      border-left: 2px solid gray;
    }

    a[href] {
      text-decoration: underline;
    }

    .tinymce { background-color:#fff }
  `]
})
export class TestTinyMCEComponent {


  constructor(private router: Router, private eventService: EventService, @Inject(JQ_TOKEN) private $: any) {

  }

  ngOnInit() {
    tinymce.init({
      selector: '#tinymce',//'div.tinymce',
      theme: 'inlite',
      plugins: 'image table link paste contextmenu textpattern autolink',
      insert_toolbar: 'quickimage quicktable',
      selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
      inline: true,
      paste_data_images: true,
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });
  }
}