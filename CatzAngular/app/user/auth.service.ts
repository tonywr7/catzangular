﻿import { Injectable } from '@angular/core'
import { IUser } from './user.model'
import { ILoginInfo } from './loginInfo.model'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AuthService {
  currentUser: IUser;

  constructor(private http: Http) { }

  loginUser(userName: string, password: string) {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let loginInfo: ILoginInfo = { username: userName, password: password };

    return this.http.post("/api/auth/login", loginInfo, options).do((response) => {
      if (response) {
//        var json = response.json();
        this.currentUser = <IUser>response.json();
      }
    }).catch(error => {
      return Observable.of(false);
    });
    //this.currentUser = {
    //  id: 1,
    //  userName: userName,
    //  firstName: 'Anthony',
    //  lastName: 'Wright'
    //};
  }

  isAuthenticated() {
    return !!this.currentUser
  }

  checkAuthenticationStatus() {
    return this.http.get('/api/auth/currentIdentity').map((response: any) => {
      if (response._body && response._body !== "null") {
        return response.json();
      }
      else {
        return {};
      }
    }).do(currentUser => {
      if (!!currentUser.userName) { // if the property exists
        this.currentUser = currentUser;
      }

    }).subscribe();

  }

  updateCurrentUser(firstName: string, lastName: string) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //return this.http.put(`/api/auth/updateCurrentUser/${this.currentUser.id}`, this.currentUser, options);
    return this.http.post("/api/auth/updateCurrentUser", this.currentUser, options);

  }

  logout() {
    this.currentUser = undefined;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post("/api/auth/logout", {}, options);
  }
}
