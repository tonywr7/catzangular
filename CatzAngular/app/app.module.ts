import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule } from '@angular/router'
import { HttpModule, JsonpModule } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { EJAngular2Module } from 'ej-angular2';

import {
  CatzService,
  CatzListResolver,
  CatzListComponent
} from './catz/index'

import {
  EventsListComponent,
  EventThumbnailComponent,
  EventService,
  EventDetailsComponent,
  CreateEventComponent,
  TestGridEventComponent,
  //EventRouteActivator, *S97* no longer used
  EventListResolver,
  EventResolver,
  CreateSessionComponent,
  SessionListComponent,
  DurationPipe,
  UpvoteComponent,
  VoterService,
  LocationValidatorDirective,
  TestSignalRComponent,
  TestTinyMCEComponent
} from './events/index'

import { EventsAppComponent } from './events-app.component'
import { NavBarComponent } from './nav/navbar.component'

//import { ToastrService } from './common/toastr.service'
import { TOASTR_TOKEN, Toastr, JQ_TOKEN, CollapsibleWellComponent, SimpleModalComponent, ModalTriggerDirective } from './common/index'

import { appRoutes } from './routes'
import { Error404Component } from './errors/404.component'

import { AuthService } from './user/auth.service'

declare let toastr: Toastr;
declare let jQuery: Object; /* no interface, so use the object type */

export declare let $: any;

// ***5***

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    EJAngular2Module.forRoot(),
    JsonpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [ /* every component and pipe added to the solution must be added here ***9*** */
    /* components */
    CatzListComponent,

    EventsAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    EventDetailsComponent,
    CreateEventComponent,
    Error404Component,
    NavBarComponent,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent,
    SimpleModalComponent,
    UpvoteComponent,
    TestGridEventComponent,
    TestSignalRComponent,
    TestTinyMCEComponent,
    /* pipes */
    DurationPipe,
    /* directives */
    ModalTriggerDirective,
    LocationValidatorDirective
  ], 
  providers: [ //providers are shared across ALL modules. You therefore only need to declare them once.  This is where all the services are added */
    CatzService,
    CatzListResolver,

    EventService,
    //ToastrService,
    { provide: TOASTR_TOKEN, useValue: toastr }, // this is how to inject third party javascript objects, together with the OpaqueToken and the @Inject decorator
    { provide: JQ_TOKEN, useValue: jQuery }, // this is how to inject third party javascript objects, together with the OpaqueToken and the @Inject decorator
    //EventRouteActivator, this was removed as it is no longer being used in this method *S97*
    { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
    EventListResolver,
    EventResolver,
    AuthService,
    VoterService
  ],
  bootstrap: [EventsAppComponent] /* this is the start component ***6*** */
})
export class AppModule { }

function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('You have not saved this event, do you really want to cancel?')
  }
  return false;
}