﻿export interface IPetOwner {
  age: number
  gender: string
  name: string
  pets: IPet[]
}

export interface IPet {
  name: string
  type: string
}

export interface ICatsByGender {
    gender: string;
    pets: string[];
}
