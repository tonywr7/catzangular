﻿import { Injectable, EventEmitter } from '@angular/core'
import { Subject, Observable } from 'rxjs/RX'
import { IPetOwner, IPet, ICatsByGender } from './index'
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http'

@Injectable()
export class CatzService {

    constructor(private http: Http, private jsonp: Jsonp) { }

    private catsByGender: ICatsByGender[] = [];

    getCatz(): Observable<ICatsByGender[]> {

        return this.jsonp.get("http://agl-developer-test.azurewebsites.net/people.json?callback=JSONP_CALLBACK").map((response: Response) => {

            var people = <IPetOwner[]>response.json();

            var catsByGender = people
                .reduce((resultArray: string[], currentItem: IPetOwner) => {
                    if (!resultArray.some(gender => gender === currentItem.gender)) resultArray.push(currentItem.gender); // if gender does not exist, add it to the resultArray
                    return resultArray;
                }, []) // this returns a unique list of genders. In reduce, the [] on this line is the initial value of the resultArray
                .map((gender: string) => {
                    return {
                        gender: gender,
                        pets: people
                            .reduce((resultArray: string[], currentItem: IPetOwner) => {
                                if (currentItem.gender === gender && currentItem.pets !== null)
                                    resultArray = resultArray.concat(
                                        currentItem.pets
                                            .reduce((resultArray: string[], currentItem: IPet) => {
                                                if (currentItem.type === "Cat") resultArray.push(currentItem.name);
                                                return resultArray;
                                            }, [])) // gets an array of cat names of an individual owner
                                return resultArray;
                            }, []) // combines all owners under a particular gender
                            .sort()
                    }
                }); // map transforms the input array into a new output array

            return catsByGender;

        }).catch(this.handleError);

    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }

}
