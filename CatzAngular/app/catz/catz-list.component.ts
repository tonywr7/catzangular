﻿import { Directive, ElementRef, Inject, Input, OnInit } from '@angular/core'
import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { IPetOwner, IPet, ICatsByGender } from './index'

@Component({
  templateUrl: 'app/catz/catz-list.component.html',
  styles: [`
    em {float:right; color:#E05C65; padding-left:10px;}
    .error input {background-color:#E3C3C5;}
    .error ::-webkit-input-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error ::-moz-placeholder {color: #999; }
    .error :ms-input-placeholder { color: #999; }
  `]
})
export class CatzListComponent implements OnInit {

  public catsByGender: ICatsByGender[];

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.catsByGender = this.route.snapshot.data['catzByGender']

  }

}