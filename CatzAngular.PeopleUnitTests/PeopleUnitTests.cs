﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using CatzAngular.Services;
using CatzAngular.FakeRepo;
using System.Linq;
using Shouldly;

namespace CatzAngular.PeopleUnitTests
{
  public class PeopleUnitTests
  {
    [Fact(DisplayName = "There should be two genders")]
    public void ThereShouldBeTwoGenders()
    {
      var peopleService = new PeopleService(new FakePeopleRepo());
      var people = peopleService.GetAll();
      var genderCount = people.Select(p => p.Gender).Distinct().Count();
      genderCount.ShouldBe(2);
    }

    [Fact(DisplayName = "There should be three males")]
    public void ThereShouldBeThreeMales()
    {
      var peopleService = new PeopleService(new FakePeopleRepo());
      var people = peopleService.GetAll();
      var maleCount = (from p in people
                       where p.Gender == "Male"
                       select p).Count();
      maleCount.ShouldBe(3);
    }

    [Fact(DisplayName = "There should be a total of 3 cats for female owners")]
    public void ThereShouldBeATotalOfThreeCatsForFemaleOwners()
    {
      var peopleService = new PeopleService(new FakePeopleRepo());
      var people = peopleService.GetAll();
      var catCount = (from person in people
                      where person.Gender == "Female"
                      select person.Pets.Where(pet => pet.Type == "Cat")).Count();
      catCount.ShouldBe(3);
    }
  }
}
