﻿using CatzAngular.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CatzAngular.Model;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace CatzAngular.Repo
{
  public class PeopleRepo : IPeopleRepo
  {

    public static readonly string peopleJsonUrl = "http://agl-developer-test.azurewebsites.net/people.json";

    public People[] GetAll()
    {
      var memoryStream = new MemoryStream();
      var req = WebRequest.CreateHttp(peopleJsonUrl);
      using (WebResponse response = req.GetResponse())
      {
        using (Stream responseStream = response.GetResponseStream())
        {
          // Read the bytes in responseStream and copy them to content. 
          responseStream.CopyTo(memoryStream);
        }
      }
      memoryStream.Position = 0;
      using (var streamReader = new StreamReader(memoryStream))
      {
        var jsonString = streamReader.ReadToEnd();
        return JsonConvert.DeserializeObject<People[]>(jsonString);
      }
    }
  }
}
