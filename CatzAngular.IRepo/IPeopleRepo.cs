﻿using CatzAngular.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatzAngular.IRepo
{
  public interface IPeopleRepo
  {
    People[] GetAll();
  }
}
